import re
import numpy as np

liczby_podst = ['zero', 'jeden', 'dwa', 'trzy', 'cztery', 'pięć', 'sześć', 'siedem', 'osiem', 'dziewięć', 'dziesięć']
liczby_odm = ['zero', 'pierwsz', 'drugi', 'trzeci', 'czwart', 'piąt', 'szóst', 'siódm', 'ósm', 'dziewiąt', 'dziesiąt']

miesiace = ['styczeń', 'luty', 'marzec', 'kwiecień', 'maj', 'czerwiec', 'lipiec', 'sierpień', 'wrzesień', 'październik', 'listopad', 'grudzień']
miesiace_odm = ['styczni', 'lut', 'marc', 'kwietni', 'maj', 'czerwc', 'lipc', 'sierpni', 'wrześni', 'październik', 'listopad', 'grudni']

def nascie(n):
    if n in [1, 4]:
        return liczby_podst[n][:-1] + 'naście'
    elif n in [5, 9]:
        return liczby_podst[n][:-1] + 'tnaście'
    elif n == 6:
        return liczby_podst[n][:-2] + 'snaście'
    else:
        return liczby_podst[n] + 'naście'
    
def dziescia(n):
    if n == 4:
        return liczby_podst[n][:-1] + 'dzieści'
    elif n == 2:
        return liczby_podst[n] + 'dzieścia'
    elif n >= 5:
        return liczby_podst[n] + 'dziesiąt'
    
    return liczby_podst[n] + 'dzieści'

def sto(n):
    if n == 1:
        return 'sto'
    elif n == 2:
        return 'dwieście'
    elif n <= 4:
        return liczby_podst[n] + 'sta'
    else: return liczby_podst[n] + 'set'
    
def tysiac(n):
    if n == 1:
        return 'tysiąc'
    else: return liczby_podst[n] + ' tysiące'
    
def odm_prepare(liczba):
    liczba = liczba.replace(' ', '')
    liczba = liczba.replace('dwa', 'dwu')
    liczba = liczba.replace('naście', 'nast')
    liczba = re.sub(r'(?:set|st[ao])', 'setn', liczba)
    liczba = re.sub(r'ieście', 'usetn', liczba)
    liczba = re.sub(r'ścia?', 'st', liczba)
    liczba = re.sub(r'siące?', 'sięczn', liczba)
    
    return liczba
    
def odmien(liczba, m):
    if m == 0 or liczba == 'zero':
        return liczba
    elif m == 1:
        return (odm_prepare(liczba) + 'y').replace('iy', 'i')
    elif m == 2:
        return odm_prepare(liczba) + 'ego'
        
def month_to_str(n, **kwargs):
    v = kwargs.get('v', np.random.randint(0, 2))
    
    if v == 0:
        return miesiace[n-1]
    elif v == 1:
        return miesiace_odm[n-1] + 'a' if n != 2 else miesiace_odm[n-1] + 'ego'
    
def num_to_str(n, add_zero=True, **kwargs):
    v = kwargs.get('v', np.random.randint(0, 3))
    
    if n == 0 and not add_zero:
        return ''

    if n <= 10:
        if v == 0:
            return liczby_podst[n]
        else: return odmien(liczby_odm[n], v)
    elif n < 20:
        liczba = nascie(n % 10)
        return odmien(liczba, v)
    elif n < 100:
        return odmien(dziescia(n // 10), v) + ' ' + num_to_str(n % 10, add_zero=False, v=v)
    elif n < 1000:
        return odmien(sto(n // 100), v) + ' ' + num_to_str(n % 100, add_zero=False, v=v)
    else:
        return odmien(tysiac(n // 1000), v) + ' ' + num_to_str(n % 1000, add_zero=True, v=v)
        
def split(s):
    split_s = []
    i = 0
    if np.random.randint(2):
        return [int(s)]
    
    while i < len(s):
        if i % 2 == 0:
            j = np.random.randint(i + 1, i + 3)
        else: j = i + 1
            
        split_s.append(int(s[i:j]))
        i = j

    return split_s

def year_generator(year, **kwargs):
    include_tysiac = kwargs.get('inc_tys', False)
    if not include_tysiac and year > 1925 and year <= 2025:
        st_index = np.random.choice([0, 2])
    else: st_index = 0
        
    if not include_tysiac:
        split_n = split(str(year)[st_index:])
    else: split_n = [year]
    
    year_str = ' '.join([num_to_str(num, **kwargs) for num in split_n])
    return year_str
    
def daymonth_generator(n, **kwargs):
    if n >= 10 or np.random.randint(2):
        split_n = [n]
    else: split_n = split('0' + str(n))
        
    daymonth_str = ' '.join([num_to_str(num, **kwargs) for num in split_n])
    
    return daymonth_str

def date_generator(**kwargs):
    day = kwargs.get('day', np.random.randint(1, 32))
    month = kwargs.get('month', np.random.randint(1, 13))
    year = kwargs.get('year', np.random.randint(1800, 2100))
    
    beginning = np.random.choice(['hmm...', 'yyy', 'to było...', 'to tak'])
    ending = np.random.choice(['chyba tak', 'tak', ''])
    
    for key in ['day', 'month', 'year']:
        if key in kwargs:
            del kwargs[key]
    
    day_str = daymonth_generator(day, **kwargs)
    if np.random.randint(2):
        month_str = month_to_str(month, **kwargs)
    else:
        month_str = daymonth_generator(month, **kwargs)
    if np.random.randint(2):
        year_str = year_generator(year, **kwargs)

        date_str = beginning + ' ' + ' '.join([day_str, month_str, year_str]) + ' ' + ending
    else:
        year_str = year_generator(year, inc_tys=True, **kwargs)

        date_str = beginning + ' ' + ' '.join([year_str, day_str, month_str]) + ' ' + ending
    
    date_str = re.sub(r'\s{2,}', ' ', date_str)
    yield date_str, (day, month, year)