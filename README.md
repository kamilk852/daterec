Przykład użycia:
```python
from daterec import recognize
recognize('yyyy rok dwa tysiące piąty zero ósmy zero czwarty')
```
Na wyjściu mamy słownik:
```
{'day': 8, 'month': 4, 'year': 2005, 'recognized': True}
```
Klucz 'recognized' informuje o tym, czy według modelu data została prawidłowo rozpoznana.