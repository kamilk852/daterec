import re
from copy import copy
import numpy as np

class NumberReplace:
    def __init__(self, m, mult):
        self.mult = mult
        self.m = m

    def __call__(self, text):
        text = text.group()

        beg_to_num = {'zero': 0, 'pie': 1, 'je': 1, 'dw': 2, 'dr': 2, 'tr': 3, 'c': 4,
                      'pią': 5, 'pię': 5, 'sz': 6, 'si': 7, 'o': 8, 'ó': 8,
                      'dziew': 9, 'dzies': 10, 'ty': 1}

        num = -1
        for beg, num in beg_to_num.items():
            if text.startswith(beg):
                break

        if self.mult:
            return str(self.m*num) + ' '
        else: return str(self.m + num) + ' '

def month_replace(text):
    text = text.group()

    beg_to_month = {'st': 1, 'lut': 2, 'mar': 3, 'kw': 4, 'maj': 5, 'cz': 6,
                    'lip': 7, 'si': 8, 'wr': 9, 'pa': 10, 'lis': 11, 'gr': 12}

    month = -1
    for beg, month in beg_to_month.items():
        if text.startswith(beg):
            break

    return str(month) + ' '

def replace(text):
    liczba = r'(?:zero|jeden?|dwa|dwu|trzy|cztery?|pię[ćt]|sze(?:ść|s)|siedem|osiem|dziewię[ćt]|dziesięć)'
    liczba_odm = r'(?:pierwsz|drugi|trzeci|czwart|piąt|szóst|siódm|ósm|dziewiąt|dziesiąt)' + \
                 r'(?:y|ego|)'

    zero_dziesiec_nieodm = liczba + r'(?:\s|$)'
    zero_dziesiec_odm = liczba_odm + r'(?:\s|$)'
    nascie = liczba + r'na[sś][a-ząęśćóżź]{2,}(?:\s|$)'
    dziesiat = liczba + r'dzie[sś][a-ząęśćóżź]{2,}(?:\s|$)'
    sto = liczba + r'set(?:n[a-z]{,3}|\s|$)'
    tysiac = r'(?:dwa\s|dwu|)tysi[ąę]ce?(?:zn[a-z]{,3}|)(?:\s|$)'
    miesiac = r'(?:stycz(?:eń|nia)|lut(?:y|ego)|mar(?:zec|ca)|kwie(?:cień|tnia)|' + \
            r'maja?|czerw(?:iec|ca)|lip(?:iec|ca)|sierp(?:ień|nia)|' + \
            r'wrze(?:sień|śnia)|października?|listopada?|grud(?:zień|nia))(?:\s|$)'

    text_ = copy(text)

    text_ = re.sub(tysiac, NumberReplace(1000, True), text_)
    text_ = re.sub(dziesiat, NumberReplace(10, True), text_)
    text_ = re.sub(zero_dziesiec_nieodm, NumberReplace(0, False), text_)
    text_ = re.sub(zero_dziesiec_odm, NumberReplace(0, False), text_)
    text_ = re.sub(nascie, NumberReplace(10, False), text_)
    text_ = re.sub(sto, NumberReplace(100, True), text_)
    text_ = re.sub(miesiac, month_replace, text_)

    nums = list(map(int, re.findall(r'\d+', text_)))
    nums = [num for i, num in enumerate(nums) if num != 0 or i >= len(nums) - 3]
    return nums

def daymonth_firsttwo(nums):
    return nums[2] in [18, 19, 20, 1000, 2000] or \
            ((nums[0] < 20 or \
            (len(nums) >= 6 and all(num < 10 for num in nums[-4:]))) and \
            len(nums) < 7 and \
            (len(nums) < 4 or nums[3] not in [18, 19, 20, 1000, 2000]))

def thousand_at_beg(nums):
    return nums[0] >= 1000

def rzad_wielkosci(num):
    if num <= 0:
        return 0
    return int(np.floor(np.log10(num) + 1))

def thousand_end_index(nums):
    if nums[0] == 1000:
        st_len = 4
    else: st_len = 3
        
    for i, num in enumerate(nums[:4]):
        if rzad_wielkosci(num) < (st_len-1-i):
            return i + 1
        if i > 0 and rzad_wielkosci(nums[i-1]) <= rzad_wielkosci(num):
            return i
        if (num >= 10 and num < 20):
            return i + 1
        
    return st_len

def get_day(nums):
    if thousand_at_beg(nums):
        nums = nums[thousand_end_index(nums):]
        if len(nums) < 2:
            return 0
        elif len(nums) == 2:
            return nums[0]
        else: return nums[0] + nums[1]
    else:
        if daymonth_firsttwo(nums):
            return nums[0]
        else: return nums[0] + nums[1]
        
def get_month(nums):
    if thousand_at_beg(nums):
        nums = nums[thousand_end_index(nums):]
        if len(nums) < 2:
            return 0
        elif len(nums) == 2:
            return nums[1]
        else: return nums[2]
    else:
        if daymonth_firsttwo(nums):
            return nums[1]
        else: return nums[2]

def get_year(nums):
    if len(nums) <= 2:
        return 0
    #print(nums)
    if thousand_at_beg(nums):
        nums = nums[:thousand_end_index(nums)]
    else:
        if daymonth_firsttwo(nums):
            nums = nums[2:]
        else: nums = nums[3:]

    #print(nums)
    
        
    for i, num in enumerate(reversed(nums)):
        if num < 10**i:
            nums[-i-1] = num * 10**(i + int(nums[-1] >= 10))

    #print(nums)
    if len(nums) == 0:
        return 0
    elif len(nums) == 1:
        if sum(nums) < 18:
            return 2000 + sum(nums)
        else: return 1900 + nums[0]
    elif nums[0] in [18, 19, 20]:
        return nums[0]*100 + nums[1]
    elif nums[0] < 100:
        
        if sum(nums) < 18:
            return 2000 + sum(nums)
        else: return 1900 + sum(nums)
    elif nums[0] < 1000:
        return nums[0]*10 + sum(nums[1:])
    else: return sum(nums)

def recognize(text):
    numbers = replace(text)
    day = get_day(numbers)
    month = get_month(numbers)
    year = get_year(numbers)
    recognized = day >= 1 and day <= 31 and month >= 1 and month <= 31 and year >= 1800 and year <= 2100
    return {'day': day, 'month': month, 'year': year, 'recognized': recognized}
